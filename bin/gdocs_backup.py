#!/usr/bin/env python
import sys
import getopt
import gdata.docs.service
import gdata.spreadsheet.service
import simplejson
import os
import xml.etree.ElementTree as ET
import datetime

##LICENSE:
#As long as you retain this notice you can do whatever you want with
#this software. If we meet some day, you should buy me a coffee.

class InfoFile:
    """ Information file

    * username
    * password
    * last updated
    """
    def __init__(self, filename):
        """Initialize and read file
        """
        self.filename = filename
        self.username = None
        self.password = None
        self.last_update = None
        self.last_run = None
        self.read()

    def read(self):
        """Read file
        """
        fileRef = open(self.filename, "r")
        line = ""
        for l in fileRef:
            line += l
        fileRef.close()
        
        c = simplejson.loads(line)
        self.username = c["username"]
        self.password = c["password"]
        if ("lastupdate" in c):
            self.last_update = isotime(c["lastupdate"])

    def write(self):
        """Write file
        """
        c = {}
        c["username"] = self.username
        c["password"] = self.password
        if (self.last_update != None):
            c["lastupdate"] = str(self.last_update.isoformat())
        if (self.last_run != None):
            c["lastrun"] = str(self.last_run.isoformat())
            
        fileRef = open(self.filename, "w")
        fileRef.write(simplejson.dumps(c, sort_keys=True, indent=4)+"\n")
        fileRef.close()
        
def get_client(login_info):
    """Get client object
    """
    client = gdata.docs.service.DocsService()
    client.ClientLogin(login_info.username,
                       login_info.password)
    return client

def isotime(dt_str):
    """Parse ISO time
    """
    dt, _, us= dt_str.partition(".")
    dt= datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
    return dt + datetime.timedelta()

def get_updated(doc):
    """Get last update time
    """
    return isotime((ET.fromstring(str(doc.updated))).text)

def get_spreadsheet(gd_client, entry, file_path):
    spreadsheets_client = gdata.spreadsheet.service.SpreadsheetsService()
    spreadsheets_client.email = gd_client.email
    spreadsheets_client.password = gd_client.password
    spreadsheets_client.source = "GDocsBackup"
    spreadsheets_client.ProgrammaticLogin()

    docs_auth_token = gd_client.GetClientLoginToken()
    gd_client.SetClientLoginToken(spreadsheets_client.GetClientLoginToken())
    gd_client.Export(entry, file_path)

def download_params(doc,outdir):
    """Download file parameters
    """
    dtype = doc.GetDocumentType()
    params = [outdir+doc.title.text, None]
    if dtype == "document":
        params[1] = "doc"
    elif dtype == "spreadsheet":
        params[1] = "xls"
    elif dtype == "presentation":
        params[1] = "ppt"
    elif dtype == "pdf":
        params[1] = "pdf"
    else:
        params.append(dtype)
        return params

    if ((len(params[0]) < (len(params[1])+1)) or
        (params[0][-(len(params[1])+1):] != "."+params[1])):
        params[0]+="."+params[1]
        
    return params 

def download(client, doc, doc_params):
    """Download file
    """
    if (doc_params[1] == "pdf"):
        client.Download(doc,
                        doc_params[0])
    elif (doc_params[1] == "xls"):
        get_spreadsheet(client, doc, doc_params[0])
    else:
        client.Download(doc,
                        doc_params[0],
                        doc_params[1])

def usage():
    """Usage information
    """
    print "Usage "+sys.argv[0]+" [options] <Info File>"
    print "\tDownload file on GDocs"
    print "Options:"
    print "-h/--help\n\tPrint this usage guide"
    print "-d/--dir\n\tDownload directory"
    print "-v/--verbose\n\tVerbose output"
    print ""
    print "<Info File>:"
    print "\tFile containing information [Default:GDocsBackup.json]"

#Get options and arguments
try:
    opts, args = getopt.getopt(sys.argv[1:], "hd:v",
                               ["help","dir","verbose"])
except getopt.GetoptError:
    print "Option error!"
    usage()
    sys.exit(2)
    
##Process options
outdir = ""
verbose=False
for opt,arg in opts:
    if (opt in ("-h","--help")):
        usage()
        sys.exit(0)
    elif (opt in ("-d","--dir")):
        outdir=arg
    elif (opt in ("-v","--verbose")):
        verbose=True
    else:
        print "Unhandled option :"+opt
        sys.exit(2)

##Get info file and service client
infofile="GDocsBackup.json"
if (len(args) >= 1):
    infofile = args[0]
info = InfoFile(infofile)
client = get_client(info)

##Get document list
documents_feed = client.GetDocumentListFeed()
maxtime = info.last_update
for doc in documents_feed.entry:
    utime = get_updated(doc)

    ##Remember latest time
    if ((maxtime == None)
        or (utime > maxtime)):
        maxtime = utime

    ##Check if file is old
    params = download_params(doc,outdir)
    if ((info.last_update == None)
        or (utime > info.last_update)
        or (not os.path.isfile(params[0]))):
        try:
            if (verbose):
                print "Downloading "+params[0]+"..."
            if params[1] == None:
                print "Unknown file type for "+params[0]+"!"
            else:
                download(client, doc, params)
        except gdata.service.RequestError:
            print "Failed to download "+params[0]+"!!!"

##Wrap up
info.last_update = maxtime
info.last_run = datetime.datetime.now()
info.write()
